nix-overlay
===========

An Nix overlay for personal packages (that I either wrote or forked):
 - [ftags](http://framagit.org/rogarb/ftags)
 - [huextract](http://github.com/rogarb/huextract)
 - [keepassxc-wrapper](http://framagit.org/rogarb/keepassxc-wrapper)
 - [nix-genpkg](http://framagit.org/rogarb/nix-genpkg)
 - [rlauncher](http://framagit.org/rogarb/rlauncher)
 - [rnotes](http://framagit.org/rogarb/rnotes)
 - [rotate-screen](http://framagit.org/rogarb/rotate-screen)
 - [todo-cli](http://framagit.org/rogarb/todo-cli)
 - [vcf-splitter](http://framagit.org/rogarb/vcf-splitter)
 - [vcf-tools](http://framagit.org/rogarb/vcf-tools)
 - [xrootmanager-rs](http://framagit.org/rogarb/xrootmanager-rs)

Usage
-----

You can use the overlay directly in your NixOS configuration with:
```
nixpkgs.overlays = [
  # ...
  (import (fetchTarball "https://framagit.org/rogarb/nix-overlay/-/archive/main/nix-overlay-main.tar.gz"))
  # ...
];
```

You can also clone the repository to some path on your disk:
```
$ git clone https://framagit.org/rogarb/nix-overlay /etc/nixos/overlay
```
Alternatively download the tarball of the main branch and unpack it:
```
$ wget -c https://framagit.org/rogarb/nix-overlay/-/archive/main/nix-overlay-main.tar.bz2

$ tar -xf nix-overlay-main.tar.bz2 /etc/nixos

# Optionally rename the folder containing the overlay
$ mv /etc/nixos/nix-overlay-main /etc/nixos/overlay
```
Finally add it to the list of `nixpkgs` overlays:
```
$ $EDITOR /etc/nixos/configuration.nix
nixpkgs.overlays = [
  # ...
  (import ./overlay/overlay.nix)
  # ...
];
```

Development
-----------
In order to build a package in the root of the repo, you can use the following command:
```
$ nix-build build.nix -A PKG_NAME
```
