{ lib, fetchgit, rustPlatform }:

rustPlatform.buildRustPackage rec {
  pname = "vcf-tools";
  version = "0.3.0";

  src = fetchgit {
    url = "https://framagit.org/rogarb/${pname}";
    rev = "refs/tags/v${version}";
    sha256 = "sha256-MhqmkMf03+8JGz+LUKfIdJPgBrAU1bwvTtT0NHMzbJ0=";
  };

  cargoSha256 = "sha256-C9n3Xilrffi8Hc4vQYAAKRbxFT+8kvURxw0YdfHccZ8=";

  meta = with lib; {
    description = "A set of CLI tools for VCARD files manipulation";
    license = licenses.gpl3;
    platforms = platforms.all;
  };
}
