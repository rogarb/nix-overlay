{ lib, rustPlatform, fetchgit, libsecret, keepassxc, ncurses, makeWrapper }:

rustPlatform.buildRustPackage rec {
  pname = "keepassxc-wrapper";
  version = "0.3.1";

  src = fetchgit {
    url = "https://framagit.org/rogarb/${pname}";
    rev = "refs/tags/v${version}";
    sha256 = "sha256-/fjvcLE0TS6bDv/dmG5berp8IJiJjahiJdWEATmskc8=";
  };
  
  buildInputs = [
    ncurses makeWrapper
  ];

  postFixup = ''
    wrapProgram $out/bin/${pname} \
      --set SECRET_TOOL_COMMAND "${libsecret}/bin/secret-tool" \
      --set KEEPASSXC_CLI_COMMAND "${keepassxc}/bin/keepassxc-cli"
  '';

  meta = {
    description = "A TUI wrapper for keepassxc-cli";
    license = lib.licenses.gpl3;
    platforms = lib.platforms.linux;
  };

  cargoSha256 = "sha256-5IJpP2s44UiMTxbdhTMnxtPh9y8ucxiCZ3Amtc0WofA=";
}
