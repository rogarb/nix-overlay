{ lib, stdenv, fetchgit, makeWrapper, nix-prefetch, python310 }:

stdenv.mkDerivation (finalAttrs: {
  pname = "nix-genpkg";
  version = "1";
  binary_name = "nix_genpkg.py";
  target_bin_name = "${finalAttrs.pname}";
  dontConfigure = true;
  dontBuild = true;
  dontFixup = true;
  
  src = fetchgit { 
    url = "https://framagit.org/rogarb/nix-genpkg"; 
    sha256 = "sha256-WWP9jjf2YAxCKkTHsjL/Yy5zHEofdb445X3d2gQK+8E=";
  };

  meta = with lib; {
    description = "A tool to automatically generate Nix expressions for package installation";
    license = licenses.gpl3;
    platforms = platforms.all;
  };
  buildInputs = [ 
    nix-prefetch
    python310
  ];
  nativeBuildInputs = [ 
    makeWrapper
  ];
  installPhase = ''
    mkdir -p $out/bin
    cp ${finalAttrs.binary_name} $out/bin/${finalAttrs.target_bin_name}
    patchShebangs --host $out/bin/${finalAttrs.target_bin_name}
    wrapProgram ${placeholder "out"}/bin/${finalAttrs.target_bin_name} \
      --prefix PATH : "${lib.makeBinPath [ nix-prefetch ]}"
  '';
})
