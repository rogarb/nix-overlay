{ lib, stdenv, fetchgit, xorg }:

stdenv.mkDerivation (finalAttrs: {
  pname = "rotate-screen";
  version = "1";
  binary_name = "rotate_screen";

  src = fetchgit {
    url = "https://framagit.org/rogarb/${finalAttrs.pname}";
    #rev = "refs/tags/v${version}";
    sha256 = "sha256-i6hrJ6cfIDe9HiOQ9DuX4MUeiKW26WOxbQc8yWnZnfE=";
  };

  meta = with lib; {
    description = "A small utility to rotate the screen using X RandR extension";
    license = licenses.gpl3;
    platforms = platforms.all;
  };
  # Fix CC variable definition in Makefile
  makeFlags = [ "CC=${stdenv.cc.targetPrefix}cc" ];

  installPhase = ''
    mkdir -p $out/bin
    cp ${finalAttrs.binary_name} $out/bin/${finalAttrs.binary_name}
  '';
  buildInputs =  builtins.attrValues {
    inherit (xorg) libX11 libXrandr;
  };
})
