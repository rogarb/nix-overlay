{ lib, fetchgit, rustPlatform, xorg }:

rustPlatform.buildRustPackage rec {
  pname = "xrootmanager";
  version = "0.2.1";

  src = fetchgit {
    url = "https://framagit.org/rogarb/xrootmanager-rs";
    rev = "refs/tags/v${version}";
    sha256 = "sha256-ZaXsu9vbqUxaEd+HcX0IynCzFu0YkN39yEtmQdw14KU=";
  };

  cargoSha256 = "sha256-fE3bw6zMY5Oo9C8frxlgowW8BqPxN9GSTdiBO8+Pt84=";

  meta = with lib; {
    description = "Display stuff on the background";
    license = licenses.gpl3;
    platforms = platforms.all;
  };
  buildInputs = builtins.attrValues {
    inherit (xorg) libxcb;
  };
}
