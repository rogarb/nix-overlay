{ lib, fetchgit, rustPlatform, cmake, curl, git, pkgconfig, pango, xorg, }:

rustPlatform.buildRustPackage rec {
  pname = "rlauncher";
  version = "0.2.0";

  src = fetchgit {
    url = "https://framagit.org/rogarb/${pname}";
    rev = "refs/tags/v${version}";
    sha256 = "sha256-RcQDHp1O13e5geHd+IZavUGbR2dKDnbVEKzeKCpehDs=";
  };

  cargoSha256 = "sha256-SiCG3lHTAwXvYN3dlLKN0JZDxdr6Kkv/DZnMFJ38wu4=";

  meta = with lib; {
    description = "Graphical launcher with CLI parameters";
    license = licenses.gpl3;
    platforms = platforms.x86_64;
  };
  nativeBuildInputs = [ cmake curl git pkgconfig ];
  buildInputs =  [ pango ] ++ builtins.attrValues {
    inherit (xorg) libxcb libXcursor libXfixes libXinerama;
  };
}
