{ lib, fetchgit, rustPlatform }:

rustPlatform.buildRustPackage rec {
  pname = "ftags";
  version = "0.1.1";

  src = fetchgit {
    url = "https://framagit.org/rogarb/${pname}";
    rev = "refs/tags/v${version}";
    sha256 = "sha256-UU4mPBeLeH2BLOWSlp6kUW5cwUGM1Iy+24m5OmVQ0OU=";
  };

  cargoSha256 = "sha256-6fJHQUjdywW+Ody+L3UtbCY3uDRWkfkp+ynhGbGhy9Q=";

  meta = with lib; {
    description = "Tag files using xattr";
    license = licenses.gpl3;
    platforms = platforms.all;
  };
}
