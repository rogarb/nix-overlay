{ lib, fetchFromGitHub, rustPlatform }:

rustPlatform.buildRustPackage rec {
  pname = "huextract";
  version = "0.2.1";

  src = fetchFromGitHub {
    owner = "rogarb";
    repo = "${pname}";
    rev = "refs/tags/v${version}";
    sha256 = "sha256-0ZEAdo3gQhyrJEe0T6BCedrsJTXP53kyijUbTFM0vCY=";
  };

  cargoSha256 = "sha256-aHcWAk0LfNR9HU5OM5TD0xaEDIkDW3tvEaqZmr5+vV4=";

  meta = with lib; {
    description = "A fast, simple, efficient Huawei firmware extractor written in Rust.";
    license = licenses.gpl3;
    platforms = platforms.all;
  };
}
