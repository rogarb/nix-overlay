{ lib, stdenv, fetchgit }:

stdenv.mkDerivation (finalAttrs: {
  pname = "vcf-splitter";
  version = "0.1";
  binary_name = "${finalAttrs.pname}.py";
  target_bin_name = "${finalAttrs.pname}";

  src = fetchgit {
    url = "https://framagit.org/rogarb/${finalAttrs.pname}";
    sha256 = "sha256-txNo7suv9LeqKVwU5PIPcYYt656NbS1InCDUL7PUmJc=";
  };

  meta = with lib; {
    description = "A small script to split VCARD files";
    license = licenses.gpl3;
    platforms = platforms.all;
  };

  installPhase = ''
    mkdir -p $out/bin
    cp ${finalAttrs.binary_name} $out/bin/${finalAttrs.target_bin_name}
  '';
})
