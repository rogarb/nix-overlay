{ lib, stdenv, fetchgit, libgit2, pkg-config }:

stdenv.mkDerivation (finalAttrs: {
  pname = "todo-cli";
  version = "1.1";
  binary_name = "${finalAttrs.pname}";
  target_bin_name = "todo";

  src = fetchgit {
    url = "https://framagit.org/rogarb/${finalAttrs.pname}";
    rev = "refs/tags/v${finalAttrs.version}";
    sha256 = "sha256-bBz9egW+LThOeFp9I1TcepEGsBpLpUF6XVXdrFmXq0M=";
  };

  meta = with lib; {
    description = "Manage a todofile";
    license = licenses.gpl3;
    platforms = platforms.all;
  };
  # Fix CC variable definition in Makefile
  makeFlags = [ "CC=${stdenv.cc.targetPrefix}cc" ];

  installPhase = ''
    mkdir -p $out/bin
    cp ${finalAttrs.binary_name} $out/bin/${finalAttrs.target_bin_name}
  '';
  buildInputs = [ libgit2 pkg-config ];
})
