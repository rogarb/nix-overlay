{ lib, fetchgit, rustPlatform, git }:

rustPlatform.buildRustPackage rec {
  pname = "rnotes";
  version = "0.2.2";

  src = fetchgit {
    url = "https://framagit.org/rogarb/${pname}";
    rev = "refs/tags/v${version}";
    sha256 = "sha256-qfsjFDstOrjlBSKgCYE5r/7pKLlqyTRC+fvg38gl5fU=";
  };

  cargoSha256 = "sha256-YZGrtGED16otB5lKLYVwbpvxxA4LOPi6BaggGrnySPE=";

  meta = with lib; {
    description = "Notefiles manager";
    license = licenses.gpl3;
    platforms = platforms.all;
  };
  buildInputs = [ git ];
}
