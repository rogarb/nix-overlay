self: super:

let
  callPackage = super.callPackage;
in {
  ftags = callPackage ./packages/ftags { };
  huextract = callPackage ./packages/huextract { };
  keepassxc-wrapper = callPackage ./packages/keepassxc-wrapper { };
  nix-genpkg = callPackage ./packages/nix-genpkg { };
  rlauncher = callPackage ./packages/rlauncher { };
  rnotes = callPackage ./packages/rnotes { };
  rotate-screen = callPackage ./packages/rotate-screen { };
  todo-cli = callPackage ./packages/todo-cli { };
  vcf-splitter = callPackage ./packages/vcf-splitter { };
  vcf-tools = callPackage ./packages/vcf-tools { };
  xrootmanager = callPackage ./packages/xrootmanager { };
}
